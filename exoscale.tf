variable "secret" {}
variable "key" {}

provider "exoscale" {
  version = "~> 0.15"
  secret  = var.secret
  key     = var.key
}


resource "exoscale_ssh_keypair" "admin" {
  name       = "terraform_exoscale"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDQmoQmw/TIb28o7KTnLOAYNAvlR6sDp3AEpgF32qznYIo1AgNLXZHNltH1wMPqURhPekva11tMMw82U5uyN0J/K1DuOwL0WGPrT3KE1xHClfDRt8Tam2Lz5DsR4ZaXVUbXv7aT9MqghHAXAFBDlOPc9WkXzvma470Bde84Xc2C7Si9K2oAvWnPK2R2JRWprRGJaYqBt8uOKeFO+XR46jTdUCkVl68VuLdvluEA7JqCBrVtthN6YCB7IIJZfX3gl1hdh6qBL7oYagu8TAFlsYU0n63tIiHDCa+GAYxk5L53pF0oKE+KhR7Jij1un0avdNpkHU/+Ei51/ioZuKluCJFXD5Nr4TYYx4HeGN7pU4y4z+scoe78ccQgLjvZPMPmPq7UojoFzwXGlNNOE0JsGG2wS1xlPGvTYE9ojfh/uZQgh3zE3NsPCFxqa00uZwC8JCp7sft89b93Zp1/uyevHERDg6QWCVsdXNcna9mXAM48HhOoZLi108OvOFzwkVAxQdX8a0VZ5h2DNoWfC6gZ7867/MFpfLx4H4UQquv5i8c/RKSo6z+2ZxnOo+IlDyGl3A88n0mQRmFVL14JC7b2dejo4vFsOpTsAgLAfH2nBKZF7SRNNSLEgOIL0PkI7nVKGoMXY77H1/mFq8M/vYTT+qUGdESx/t6u2bQNtG6Sehe/lQ== exoscale"
}



data "exoscale_compute_template" "ubuntu" {
  zone = "ch-gva-2"
  name = "Linux Ubuntu 18.04 LTS 64-bit"
}

data "template_file" "ansible-hosts-template" {
  template = "${file("ansible/hosts.tpl")}"
  vars = {
    ip = exoscale_compute.instance1.ip_address
  }
  depends_on = [exoscale_compute.instance1]
}


resource "exoscale_security_group" "common" {
  name        = "common"
  description = "The common security groupe that contains the higher level of security rules"
}


resource "exoscale_security_group_rule" "ssh" {
  type              = "INGRESS"
  security_group_id = exoscale_security_group.common.id
  protocol          = "TCP"
  start_port        = 22
  end_port          = 22
  cidr              = "0.0.0.0/0"
}

resource "exoscale_security_group_rule" "web" {
  type              = "INGRESS"
  security_group_id = exoscale_security_group.common.id
  protocol          = "TCP"
  start_port        = 80
  end_port          = 80
  cidr              = "0.0.0.0/0"
}


resource "exoscale_compute" "instance1" {
  zone            = "ch-gva-2"
  display_name    = "instance1"
  template_id     = data.exoscale_compute_template.ubuntu.id
  size            = "Micro"
  disk_size       = 10
  keyboard        = "fr-ch"
  key_pair        = "terraform_exoscale"
  state           = "Running"
  security_groups = ["common"]

  connection {
    type = "ssh"
    host = self.ip_address
    user = data.exoscale_compute_template.ubuntu.username
  }

  provisioner "file" {
    source      = "wait-for-it.sh"
    destination = "wait-for-it.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x wait-for-it.sh",
      "./wait-for-it.sh"
    ]
  }
}
resource "local_file" "ansible-hosts-file" {
  content  = data.template_file.ansible-hosts-template.rendered
  filename = "ansible/hosts"
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "ssh-keyscan ${exoscale_compute.instance1.ip_address}  >> ~/.ssh/known_hosts && ansible-playbook -i ansible/hosts ansible/playbook.yml"
  }
  triggers = {
    "after" = local_file.ansible-hosts-file.id,
  }
}
