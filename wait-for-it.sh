#!/usr/bin/env bash

for(( ; ; ))
do
  nbLine="$(ps aux | grep -i apt | wc -l)";
  limite=1
  if (("$nbLine" == "$limite")); then
    break
  fi;
  echo "/var/lib/dpkg/ locked, sleeping 10sec"
  sleep 5
done
